Les utilisateurs peuvent se connecter en utilisant leur email et mot de passe. 
Affichage de tous les utilisateurs avec leurs détails : ID, nom, email, occupation, et bio. 
Possibilité de filtrer les utilisateurs par email, nom, occupation et bio. 
Liste des Utilisateurs avec Pagination : 
Affiche les détails complets d'un utilisateur sélectionné. 


Les utilisateurs peuvent être mis à jour en modifiant leurs informations telles que le nom, l'email, l'occupation, et la bio. 
Possibilité d'ajouter de nouveaux utilisateurs à la liste. 
Les utilisateurs peuvent être supprimés de la liste. 
